﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logger
{
    public interface ILoggerManager
    {
        void LogInfo(string message);
        void LogInfo(string message, string exception);
        void LogWarn(string message);
        void LogWarn(string message, string message2);
        void LogDebug(string message);
        void LogError(string message);
    }

    public interface ILoggerManager<T>
    {
        void LogInfo(string message);
        void LogInfo(string message, string exception);
        void LogWarn(string message);
        void LogWarn(string message, string message2);
        void LogDebug(string message);
        void LogError(string message);
    }
}
