﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class r3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "created_on",
                table: "loginaccount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "created_on",
                table: "loginaccount",
                rowVersion: true,
                nullable: false,
                defaultValue: new byte[] {  });
        }
    }
}
