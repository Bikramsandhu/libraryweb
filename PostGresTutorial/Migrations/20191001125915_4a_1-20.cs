﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class _4a_120 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "loginaccount",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "uniqueusername",
                table: "loginaccount",
                columns: new[] { "username", "Guid", "Category" },
                unique: true,
                filter: "[Category] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "uniqueusername",
                table: "loginaccount");

            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "loginaccount",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
