﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class _2a_120 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "departmentName",
                table: "Department",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Department_departmentName",
                table: "Department",
                column: "departmentName",
                unique: true,
                filter: "[departmentName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Department_departmentName",
                table: "Department");

            migrationBuilder.AlterColumn<string>(
                name: "departmentName",
                table: "Department",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
