﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class q : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "teacherInfo",
                table: "loginaccount");

            migrationBuilder.DropIndex(
                name: "logininfoteacher",
                table: "loginaccount");

            migrationBuilder.DropIndex(
                name: "logininfo",
                table: "loginaccount");

            migrationBuilder.DropColumn(
                name: "teacherid",
                table: "loginaccount");

            migrationBuilder.AddColumn<int>(
                name: "LoginaccountUserid",
                table: "teacher",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_teacher_LoginaccountUserid",
                table: "teacher",
                column: "LoginaccountUserid");

            migrationBuilder.AddForeignKey(
                name: "FK_teacher_loginaccount_LoginaccountUserid",
                table: "teacher",
                column: "LoginaccountUserid",
                principalTable: "loginaccount",
                principalColumn: "userid",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_teacher_loginaccount_LoginaccountUserid",
                table: "teacher");

            migrationBuilder.DropIndex(
                name: "IX_teacher_LoginaccountUserid",
                table: "teacher");

            migrationBuilder.DropColumn(
                name: "LoginaccountUserid",
                table: "teacher");

            migrationBuilder.AddColumn<int>(
                name: "teacherid",
                table: "loginaccount",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "logininfoteacher",
                table: "loginaccount",
                column: "teacherid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "logininfo",
                table: "loginaccount",
                columns: new[] { "username", "password", "email", "teacherid" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "teacherInfo",
                table: "loginaccount",
                column: "teacherid",
                principalTable: "teacher",
                principalColumn: "teacherid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
