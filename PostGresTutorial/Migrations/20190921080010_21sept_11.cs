﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class _21sept_11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "loginaccount",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "loginaccount",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
