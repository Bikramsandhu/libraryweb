﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGresTutorial.Migrations
{
    public partial class _3a_120 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_teacher_loginaccount_LoginaccountUserid",
                table: "teacher");

            migrationBuilder.DropIndex(
                name: "IX_teacher_LoginaccountUserid",
                table: "teacher");

            migrationBuilder.DropColumn(
                name: "LoginaccountUserid",
                table: "teacher");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LoginaccountUserid",
                table: "teacher",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_teacher_LoginaccountUserid",
                table: "teacher",
                column: "LoginaccountUserid");

            migrationBuilder.AddForeignKey(
                name: "FK_teacher_loginaccount_LoginaccountUserid",
                table: "teacher",
                column: "LoginaccountUserid",
                principalTable: "loginaccount",
                principalColumn: "userid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
