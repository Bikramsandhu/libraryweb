﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PostGresTutorial.Model
{
    public partial class Book
    {
        public Book()
        {
            Issuebook = new HashSet<Issuebook>();
        }

        public int Bookid { get; set; }
        public string Bookname { get; set; }
        public string AuthorName { get; set; }
        public string Publishername { get; set; }
        public string Booktype { get; set; }
        public int Bookcopynumber { get; set; }
        [Required]
        public bool available { get; set; } = true;
		

        public ICollection<Issuebook> Issuebook { get; set; }
    }
}
