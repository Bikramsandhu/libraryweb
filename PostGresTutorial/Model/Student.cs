﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;

namespace PostGresTutorial.Model
{
    public partial class Student
    {
        public Student()
        {
            Issuebook = new HashSet<Issuebook>();
        }

        public int Studentid { get; set; }
        public string Studentfirstname { get; set; }
        public string Studentmiddlename { get; set; }
        public string Studentlastname { get; set; }
        public string Gender { get; set; }
        public string Emailaddress { get; set; }
        public string Mobilenumber { get; set; }
        public string Homenumber { get; set; }
        public string Fatherfirstname { get; set; }
        public string Fathermiddlename { get; set; }
        public string Fatherlastname { get; set; }
        public string Motherfirstname { get; set; }
        public string Mothermiddlename { get; set; }
        public string Motherlastname { get; set; }
        public string Parentmobilecontact { get; set; }
        public string Parenthomecontact { get; set; }
        public string Address { get; set; }
        public string Village { get; set; }
        public string Note { get; set; }
        public string Section { get; set; }
        public string Classname { get; set; }
        public int Rollnumber { get; set; }
        public DateTime Dob { get; set; }
       
        public Class Class { get; set; }
        public ICollection<Issuebook> Issuebook { get; set; }
		[Required]
		public Guid Guid { get; set; }
		//public Loginaccount Loginaccount { get; set; }
    }
}
