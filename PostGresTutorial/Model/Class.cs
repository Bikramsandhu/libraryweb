﻿using System;
using System.Collections.Generic;

namespace PostGresTutorial.Model
{
    public partial class Class
    {
        public Class()
        {
            Student = new HashSet<Student>();
        }

        public int Classid { get; set; }
        public string Section { get; set; }
        public string Classname { get; set; }
        public int Capacity { get; set; }

        public ICollection<Student> Student { get; set; }
		public List<ClassTeacher> classTeachers { get; set; }
	}
}
