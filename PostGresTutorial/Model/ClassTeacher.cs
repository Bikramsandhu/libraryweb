﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGresTutorial.Model
{
	public class ClassTeacher
	{
		public int teacherId { get; set; }
		public Teacher Teacher { get; set; }

		public string ClassName { get; set; }
		public string Section { get; set; }

		public string Category { get; set; }
		public Class Class { get; set; }
	}
}
