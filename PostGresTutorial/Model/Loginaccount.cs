﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PostGresTutorial.Model
{
    public partial class Loginaccount
    {
        public int Userid { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }		
		public DateTime CreatedOn { get; set; }
        public DateTime LastLogin { get; set; }	
		public string Category { get; set; }
		[Required]
		public Guid Guid { get; set; }
    }
}
