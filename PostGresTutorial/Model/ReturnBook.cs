﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PostGresTutorial.Model
{
    public class ReturnBook
    {
        [Key]
        public int ReturnId { get; set; }

        //[Required]
        //[Column(TypeName = "date")]
        //public DateTime Issuedbookdate { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime Returnbookdate { get; set; }

        //[Required]
        //public string Note { get; set; }

        //[Required]
        //public string Studentfirstname { get; set; }

        //[Required]
        //public string Studentlastname { get; set; }

        [Required]
        public string Bookname { get; set; }

        [Required]
        public int Bookcopynumber { get; set; }

        //[Required]
        //public int Rollnumber { get; set; }

        //[Required]
        //public string Classname { get; set; }

        //[Required]
        //public string Section { get; set; }

        //[Required]
        //public int IssuedTeacherid { get; set; }

        [Required]
        public int AcceptingTeacherid { get; set; }
    }
}
