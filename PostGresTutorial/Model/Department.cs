﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGresTutorial.Model
{
	public class Department
	{
		public int DepartmentId { get; set; }
		public string departmentName { get; set; }
		
		public ICollection<Teacher> teachers { get; set; }
	}
}
