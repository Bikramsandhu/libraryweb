﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PostGresTutorial.Model
{
    public partial class Teacher
    {
        public int Teacherid { get; set; }
        public string Teacherfirstname { get; set; }
        public string Teachermiddlename { get; set; }
        public string Teacherlastname { get; set; }
        public string Gender { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        public string Emailaddress { get; set; }
        public string Mobilenumber { get; set; }
        public string Homenumber { get; set; }
        public string Closerelativename { get; set; }
        public string Closerelativecontact { get; set; }
        public string Address { get; set; }
        public string Village { get; set; }
        public string Note { get; set; }

        //public Loginaccount Loginaccount { get; set; }
		public List<ClassTeacher> classTeachers { get; set; }
		[Required]
		public Guid Guid { get; set; }
		
		public int DepartmentId { get; set; }
		public Department Department { get; set; }
	}
}
