﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PostGresTutorial.Model
{
    public partial class libraryContext : DbContext
    {
        public libraryContext()
        {
        }

        public libraryContext(DbContextOptions<libraryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Class> Class { get; set; }
        public virtual DbSet<Issuebook> Issuebook { get; set; }
        public virtual DbSet<Loginaccount> Loginaccount { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<Teacher> Teacher { get; set; }

        public virtual DbSet<ReturnBook> ReturnBooks { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=BIKRAM-DESKTOP; Database=library; User=sa; Password=Bikramsandhu1;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>(entity =>
            {
                entity.ToTable("book");

                entity.HasIndex(e => new { e.Bookname, e.Bookcopynumber })
                    .HasName("bookCopyNum")
                    .IsUnique();

                entity.Property(e => e.Bookid).HasColumnName("bookid");

                entity.Property(e => e.AuthorName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bookcopynumber).HasColumnName("bookcopynumber");

                entity.Property(e => e.Bookname)
                    .IsRequired()
                    .HasColumnName("bookname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Booktype)
                    .IsRequired()
                    .HasColumnName("booktype")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Publishername)
                    .IsRequired()
                    .HasColumnName("publishername")
                    .HasMaxLength(50)
                    .IsUnicode(false);

				entity.Property(e => e.available)
				  .IsRequired()
				  .HasColumnName("available")
			      .HasDefaultValue(true);
			});

            modelBuilder.Entity<Class>(entity =>
            {
                entity.ToTable("class");

                entity.HasIndex(e => new { e.Section, e.Classname })
                    .HasName("_classnameSection")
                    .IsUnique();

                entity.Property(e => e.Classid).HasColumnName("classid");

                entity.Property(e => e.Capacity).HasColumnName("capacity");

                entity.Property(e => e.Classname)
                    .IsRequired()
                    .HasColumnName("classname")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Section)
                    .IsRequired()
                    .HasColumnName("section")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Issuebook>(entity =>
            {
                entity.ToTable("issuebook");

                entity.Property(e => e.Issuebookid).HasColumnName("issuebookid");

                entity.Property(e => e.Bookcopynumber).HasColumnName("bookcopynumber");

                entity.Property(e => e.Bookname)
                    .IsRequired()
                    .HasColumnName("bookname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Classname)
                    .HasColumnName("classname")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Issuebookdate)
                    .HasColumnName("issuebookdate")
                    .HasColumnType("date");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Returnbookdate)
                    .HasColumnName("returnbookdate")
                    .HasColumnType("date");

                entity.Property(e => e.Rollnumber).HasColumnName("rollnumber");

                entity.Property(e => e.Section)
                    .HasColumnName("section")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Studentfirstname)
                    .HasColumnName("studentfirstname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Studentlastname)
                    .HasColumnName("studentlastname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Teacherid).HasColumnName("teacherid");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.Issuebook)
                    .HasPrincipalKey(p => new { p.Bookname, p.Bookcopynumber })
                    .HasForeignKey(d => new { d.Bookname, d.Bookcopynumber })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("bookColumns");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Issuebook)
                    .HasPrincipalKey(p => new { p.Studentfirstname, p.Studentlastname, p.Classname, p.Section, p.Rollnumber })
                    .HasForeignKey(d => new { d.Studentfirstname, d.Studentlastname, d.Classname, d.Section, d.Rollnumber })
                    .HasConstraintName("_studentInfo");
            });

            modelBuilder.Entity<Loginaccount>(entity =>
            {
                entity.HasKey(e => e.Userid);

                entity.ToTable("loginaccount");

                //entity.HasIndex(e => e.Teacherid)
                //    .HasName("logininfoteacher")
                //    .IsUnique();

                //entity.HasIndex(e => new { e.Username, e.Password, e.Email, e.Teacherid })
                //    .HasName("logininfo")
                //    .IsUnique();

				entity.HasIndex(e => new { e.Username })
				.HasName("uniqueUsername")
				.IsUnique();

                entity.Property(e => e.Userid).HasColumnName("userid");

                //entity.Property(e => e.CreatedOn)
                //    .IsRequired()
                //    .HasColumnName("created_on")
                //    .IsRowVersion();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastLogin)
                    .HasColumnName("last_login")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                //entity.Property(e => e.Teacherid).HasColumnName("teacherid");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                //entity.HasOne(d => d.Teacher)
                //    .WithOne(p => p.Loginaccount)
                //    .HasForeignKey<Loginaccount>(d => d.Teacherid)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("teacherInfo");
            });

			modelBuilder.Entity<Loginaccount>(entity =>
			{
				entity.ToTable("loginaccount");

				entity.HasIndex(e => new { e.Username, e.Guid, e.Category })
				.HasName("uniqueusername")
				.IsUnique(true);
			});

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("student");

                entity.HasIndex(e => new { e.Studentfirstname, e.Studentlastname, e.Classname, e.Section, e.Rollnumber })
                    .HasName("studentuniqueInfo")
                    .IsUnique();

                entity.Property(e => e.Studentid).HasColumnName("studentid");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Classname)
                    .IsRequired()
                    .HasColumnName("classname")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.Emailaddress)
                    .IsRequired()
                    .HasColumnName("emailaddress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fatherfirstname)
                    .IsRequired()
                    .HasColumnName("fatherfirstname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fatherlastname)
                    .IsRequired()
                    .HasColumnName("fatherlastname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fathermiddlename)
                    .HasColumnName("fathermiddlename")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnName("gender")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Homenumber)
                    .IsRequired()
                    .HasColumnName("homenumber")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilenumber)
                    .IsRequired()
                    .HasColumnName("mobilenumber")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Motherfirstname)
                    .IsRequired()
                    .HasColumnName("motherfirstname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Motherlastname)
                    .IsRequired()
                    .HasColumnName("motherlastname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Mothermiddlename)
                    .HasColumnName("mothermiddlename")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Parenthomecontact)
                    .IsRequired()
                    .HasColumnName("parenthomecontact")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Parentmobilecontact)
                    .IsRequired()
                    .HasColumnName("parentmobilecontact")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Rollnumber).HasColumnName("rollnumber");

                entity.Property(e => e.Section)
                    .IsRequired()
                    .HasColumnName("section")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Studentfirstname)
                    .IsRequired()
                    .HasColumnName("studentfirstname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Studentlastname)
                    .IsRequired()
                    .HasColumnName("studentlastname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Studentmiddlename)
                    .HasColumnName("studentmiddlename")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Village)
                    .IsRequired()
                    .HasColumnName("village")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Student)
                    .HasPrincipalKey(p => new { p.Section, p.Classname })
                    .HasForeignKey(d => new { d.Section, d.Classname })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("classInfo");
            });

			modelBuilder.Entity<Department>(entity =>
			{
				entity.HasIndex(d => d.departmentName)
				.IsUnique();
			});

            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.ToTable("teacher");

                entity.HasIndex(e => new { e.Teacherfirstname, e.Teachermiddlename, e.Teacherlastname, e.Emailaddress, e.Mobilenumber, e.Homenumber })
                    .HasName("AK_teacher")
                    .IsUnique();

                entity.Property(e => e.Teacherid).HasColumnName("teacherid");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Closerelativecontact)
                    .IsRequired()
                    .HasColumnName("closerelativecontact")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Closerelativename)
                    .IsRequired()
                    .HasColumnName("closerelativename")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Emailaddress)
                    .IsRequired()
                    .HasColumnName("emailaddress")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnName("gender")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Homenumber)
                    .IsRequired()
                    .HasColumnName("homenumber")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Mobilenumber)
                    .IsRequired()
                    .HasColumnName("mobilenumber")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Teacherfirstname)
                    .IsRequired()
                    .HasColumnName("teacherfirstname")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Teacherlastname)
                    .IsRequired()
                    .HasColumnName("teacherlastname")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Teachermiddlename)
                    .IsRequired()
                    .HasColumnName("teachermiddlename")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Village)
                    .IsRequired()
                    .HasColumnName("village")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

			modelBuilder.Entity<ClassTeacher>()
				.HasKey(p => new { p.teacherId, p.ClassName,p.Section });

			modelBuilder.Entity<ClassTeacher>()
				.HasOne(p => p.Teacher)
				.WithMany(p => p.classTeachers)
				.HasForeignKey(p => p.teacherId);

			//entity.HasOne(d => d.Class)
			//		.WithMany(p => p.Student)
			//		.HasPrincipalKey(p => new { p.Section, p.Classname })
			//		.HasForeignKey(d => new { d.Section, d.Classname })
			//		.OnDelete(DeleteBehavior.ClientSetNull)
			//		.HasConstraintName("classInfo");

			//modelBuilder.Entity<ClassTeacher>()
			//	.WithMany(p => p.cl)
			//	)
		}
    }
}
