﻿using System;
using System.Collections.Generic;

namespace PostGresTutorial.Model
{
    public partial class Issuebook
    {
        public int Issuebookid { get; set; }
        public DateTime Issuebookdate { get; set; }
        public DateTime Returnbookdate { get; set; }
        public string Note { get; set; }
        public string Studentfirstname { get; set; }
        public string Studentlastname { get; set; }
        public string Bookname { get; set; }
        public int Bookcopynumber { get; set; }
        public int Rollnumber { get; set; }
        public string Classname { get; set; }
        public string Section { get; set; }
        public int Teacherid { get; set; }

        public Book Book { get; set; }
        public Student Student { get; set; }
    }
}
