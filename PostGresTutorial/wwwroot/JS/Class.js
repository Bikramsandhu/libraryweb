﻿const uri = "api/Class";
let classes = null;

function getCount(data) {
    const el = $("#counter");
    let name = "Classes";
    if (data) {
        if (data > 1) {
            name = "classes";
        }
        el.text(data + " " + name);
    } else {
        el.text("No " + name);
    }
}

$(document).ready(function () {
    getData();
});

function getData() {
    $.ajax({
        type:"GET",
        url: uri,
        cache: false,

        success: function (data) {
            const tBody = $("#classTable");
            $(tBody).empty();
            getCount(data.length);

            $.each(data, function (key, schoolclass) {
                const tr = $("<tr></tr>")
                    .append($("<td></td>").text(schoolclass.classid))
                    .append($("<td></td>").text(schoolclass.classname))
                    .append($("<td></td>").text(schoolclass.section))
                    .append($("<td></td>").text(schoolclass.capacity))
                    .append(
                        $("<td></td>").append(
                        $("<button data-toggle='modal' data-target='#editclassModel'>Edit</button>").on("click", function () {
                           
                            editClass(schoolclass.classid);
                            $('#editclassModel').modal('show');
                            })
                        )
                    )
                    .append(
                        $("<td></td>").append(
                            $("<button>Delete</button>").on("click", function () {
                            deleteClass(schoolclass.classid);
                            })
                        )
                    );
                tr.appendTo(tBody);
                $(document).ready(function () {
                    $('#table_id').DataTable();
                });
            });
            classes = data;
        }
    });
}


function deleteClass(id) {
    alertify.confirm("Are you sure, you want to delete this class?", function () { alertify.success('Class is deleted.', deleteClass()), function () { alertify.error('Cancel') } });

    function deleteClass() {
        $.ajax({
            url: uri + "/" + id,
            type: "DELETE",
            success: function (result) {
                getData();
            },
            error: function () {
                alert("Class cannot be deleted.");
            }
        });
    } 
}


function editClass(id) {
    $.each(classes, function (key,schoolclass) {
        if (schoolclass.classid === id) {
            $("#editclassid").val(schoolclass.classid);
            $("#editclassname").val(schoolclass.classname);
            $("#editsection").val(schoolclass.section);
            $("#editcapacity").val(schoolclass.capacity);
        }
    });  
}


function editclassform() {
    const schoolclass = {
        classid: $("#editclassid").val(),
        classname: $("#editclassname").val(),
        section: $("#editsection").val(),
        capacity: $("#editcapacity").val()      
    };

    try {

        if (editcapacity.value == "") {
            alertify.alert('Capcity Alert','Please enter the number of students in the class');
            capacity.focus();
            return false;
        }

        if (isNaN(editcapacity.value)) {
            alertify.alert('Capcity Alert',"Please enter a number for student value.");
            capacity.focus();
            return false;
        }

        if (editcapacity.value > 50) {
            alertify.alert('Capcity Alert',"A class cannot have more than 50 students.");
            capacity.focus();
            return false;
        }

        $.ajax({
            url: uri + "/" + $("#editclassid").val(),
            type: "PUT",
            accepts: "application/json",
            contentType: "application/json",
            data: JSON.stringify(schoolclass),
            success: function (result) {
                getData();
                $("#editclassModel .close").click()
                alertify.success('Class has been modified.'); 
            }
        });
    }
    catch (exception) {
        alert(exception);
    }        
}

function formvalues() {
    const addnclass = {
        //classid: $("#classID").val(),
        classname: $("#classname").val(),
        section: $("#section").val(),
        capacity: $("#capacity").val()  
    };

    try {

        if (capacity.value == "") {
            alertify.alert('Capcity Alert',"Please enter the number of students in the class");
            capacity.focus();
            return false;
        }

        if (isNaN(capacity.value)) {
            alertify.alert('Capcity Alert',"Please enter a number");
            capacity.focus();
            return false;
        }

        if (capacity.value > 50) {
            alertify.alert('Capcity Alert',"A class cannot have more than 50 students.");
            capacity.focus();
            return false;
        }

        $.ajax({
            type: "POST",
            url: uri,
            data: JSON.stringify(addnclass),
            accepts: "application/json",
            contentType: "application/json",
            success: function (result) {
                $("#addnewclassModel .close").click()
                getData();
                alertify.success('Class is added to table.'); 
            },
            error: function (jqXHR) {
                alert(jqXHR.responseText);
            }
        });
    }
    catch (exception) {
        alert(exception);
    }        
}

