﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	[Authorize(Policy= "RequireAdmin")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        private readonly libraryContext _context;

        public TeachersController(libraryContext context)
        {
            _context = context;
        }

        // GET: api/Teachers
        [HttpGet]
        public IEnumerable<Teacher> GetTeacher()
        {
            return _context.Teacher;
        }

        // GET: api/Teachers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTeacher([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var teacher = await _context.Teacher.FindAsync(id);

            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(teacher);
        }

        // PUT: api/Teachers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeacher([FromRoute] int id, [FromBody] Teacher teacher)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != teacher.Teacherid)
            {
                return BadRequest();
            }

            _context.Entry(teacher).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeacherExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

		
		[HttpPost]
        public async Task<IActionResult> PostTeacher([FromBody] Teacher teacher)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Teacher.Add(teacher);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TeacherExists(teacher.Teacherid))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTeacher", new { id = teacher.Teacherid }, teacher);
        }


        // DELETE: api/Teachers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeacher([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var teacher = await _context.Teacher.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }

            _context.Teacher.Remove(teacher);
            await _context.SaveChangesAsync();

            return Ok(teacher);
        }

        private bool TeacherExists(int id)
        {
            return _context.Teacher.Any(e => e.Teacherid == id);
        }

		[Authorize(Policy = "RequireTeacher")]
		[HttpGet]
        [Route("SearchWithFNLNDOB/{firstName}/{lastName}/{DOB}")]
        public async Task<IActionResult> SearchWithFNLNDOB(string firstName, string lastName, DateTime DOB)
        {
            var teacher = _context.Teacher.Where(t => t.Teacherfirstname == firstName && t.Teacherlastname == lastName && t.DOB == DOB).ToList();

            int numbers = teacher.Count();

            if (numbers != 0)
            {
                try
                {
                    return Ok(teacher);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
            else
            {
                return Content("The teacher does not exist with name " + firstName + " " + lastName + " and " + DOB + ".");
            }
        }

		[Authorize(Policy = "RequireTeacher")]
		[HttpGet]
        [Route("SearchFNLN/{firstName}/{lastName}")]
        public async Task<IActionResult> SearchFNLN(string firstName, string lastName)
        {
            IList<Teacher> teacher = _context.Teacher.Where(t => t.Teacherfirstname == firstName && t.Teacherlastname == lastName).ToList();

            int numbers = teacher.Count();

            if (numbers != 0)
            {
                try
                {
                    return Ok(teacher);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
            else
            {
                return Content("The teacher does not exist with name " + firstName + " " + lastName +".");
            }
        }

    }
}