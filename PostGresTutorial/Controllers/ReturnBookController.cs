﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	[Authorize(Policy = "RequireLibrarian")]
	[Route("api/[controller]")]
    [ApiController]
    public class ReturnBookController : ControllerBase
    {
        libraryContext libraryContext = new libraryContext();

        public ReturnBookController(libraryContext DBContext)
        {
            DBContext = libraryContext;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReturnBook>>> AllReturnedBooks()
        {
            try
            {
                var returnBooks = libraryContext.ReturnBooks.ToList();
                int numbers = returnBooks.Count();

                if (numbers != 0)
                {
                    return returnBooks;
                }
                else
                {
                    return Content("Currently, there is no issue book.");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> ReturnBook(ReturnBook returnBook)
        {
            try
            {
                var IssueBookExist = libraryContext.Issuebook.Where(b => b.Bookname == returnBook.Bookname && b.Bookcopynumber == returnBook.Bookcopynumber).ToList();
                if (IssueBookExist.Count == 1) {
                    var ID = libraryContext.Issuebook.Where(i => i.Bookname == returnBook.Bookname && i.Bookcopynumber == returnBook.Bookcopynumber).FirstOrDefault();
                    if (ModelState.IsValid)
                    {
                        libraryContext.Issuebook.Remove(ID);

                        //change the value of available field to true, so book is available
                        var bookInfo = libraryContext.Book.Where(b => b.Bookname == returnBook.Bookname && b.Bookcopynumber == returnBook.Bookcopynumber && b.available == false).FirstOrDefault();
                        libraryContext.Book.Attach(bookInfo);
                        bookInfo.available = true;
                        libraryContext.Entry(bookInfo).Property(a => a.available).IsModified = true;

                        libraryContext.ReturnBooks.Add(returnBook);

                        await libraryContext.SaveChangesAsync();
                        return Content("Thank you for returning a book.");
                    }
                    else
                    {
                        return Content("Please enter the correct value of return book.");
                    }
                }
                else
                {
                    return Content("This book cannot be returned as it hasn't been issued.");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
            
        }
    }
}