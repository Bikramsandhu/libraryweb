﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventBus1.Abstractions;
using EventBus1.Events;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PostGresTutorial.Event;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	[Authorize(Policy= "RequireLibrarian")]
    [Route("api/[controller]")]
    [ApiController]
    public class IssueBookController : ControllerBase
    {
        libraryContext _libraryContext = new libraryContext();
        private readonly IIsseBookIntegrationEventService _isseBookIntegrationEventService;
        private readonly LibrarySettings _settings;

		public IssueBookController(libraryContext context, IOptionsSnapshot<LibrarySettings> settings, IIsseBookIntegrationEventService isseBookIntegrationEventService)
		{
			_libraryContext = context ?? throw new ArgumentNullException(nameof(context));
			_settings = settings.Value;
			_isseBookIntegrationEventService = isseBookIntegrationEventService ?? throw new ArgumentNullException(nameof(isseBookIntegrationEventService));

			context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
		}

		public IssueBookController(libraryContext context, IOptionsSnapshot<LibrarySettings> settings)
		{
			_libraryContext = context ?? throw new ArgumentNullException(nameof(context));
			_settings = settings.Value;
		}

		[HttpGet]
        public async Task<ActionResult<IEnumerable<Issuebook>>> AllIssueBooks()
        {
            try
            {
                var issuebooks = _libraryContext.Issuebook.ToList();
                int numbers = issuebooks.Count();

                if (numbers != 0)
                {
                    return issuebooks;
                }
                else
                {
                    return Content("Currently, there is no issue book.");
                }
            }catch(Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetIssueBookBetweenDates/{StarttDate}/{endDate}")]
        public async Task<ActionResult<IEnumerable<Issuebook>>> GetIssueBookBetweenDates(DateTime StarttDate, DateTime endDate)
        {
        
            try
            {
                var issuedBooks = _libraryContext.Issuebook.Where(d => d.Issuebookdate >= StarttDate.Date && d.Issuebookdate <= endDate.Date).ToList();
                int count = issuedBooks.Count();

                if(count > 0)
                {
                    return issuedBooks;
                }
                else
                {
                    return Content("No Book was issued between this date");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpGet]
        [Route("GetByReturnDate/{returnDate}")]
        public async Task<IActionResult> GetByReturnDate(DateTime returnDate)
        {
            try
            {
                var returnbooks = _libraryContext.Issuebook.Where(r => r.Returnbookdate == returnDate).ToList();

                int count = returnbooks.Count();

                if(count > 0)
                {
                    return Ok(returnbooks);
                }
                else
                {
                    return Content("There is no book expected to return today.");
                }
            }
            catch(Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpGet]
        [Route("FindIssuedBookByNames/{BookName}")]
        public async Task<IActionResult> FindIssuedBookByNames(string BookName)
        {
            try
            {
                IList<Issuebook> issuedBooksByName = _libraryContext.Issuebook.Where(n => n.Bookname == BookName).ToList();

                int count = issuedBooksByName.Count();

                if(count > 0)
                {
                    return Ok(issuedBooksByName);
                }
                else
                {
                    return Content("There is no book with this name " + BookName);
                }
            }
            catch(Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpGet]
        [Route("FindIssuedBookByStudentFNLN/{firstname}/{lastname}")]
        public async Task<IActionResult> FindIssuedBookByStudentFNLN(string firstname, string lastname)
        {
            try
            {
                IList<Issuebook> IssuedBooksStudent = _libraryContext.Issuebook.Where(n => n.Studentfirstname == firstname && n.Studentlastname == lastname).ToList();

                int count = IssuedBooksStudent.Count();

                if(count > 0)
                {
                    return Ok(IssuedBooksStudent);
                }
                else
                {
                    return Content("No book is issued to the student with name of " + firstname + " " + lastname + ".");
                }
            }catch(Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        [HttpPost]
        public async Task<IActionResult> IssueBookPost(Issuebook issuebook)
        {
            try
            {
                var student = _libraryContext.Student.Where(s => s.Studentfirstname == issuebook.Studentfirstname && s.Studentlastname == issuebook.Studentlastname
                && s.Classname == issuebook.Classname && s.Section == issuebook.Section && s.Rollnumber == issuebook.Rollnumber).ToList();

                var book = _libraryContext.Book.Where(b => b.Bookname == issuebook.Bookname && b.Bookcopynumber == issuebook.Bookcopynumber && b.available == true).ToList();

                var teacher = _libraryContext.Teacher.Where(t => t.Teacherid == issuebook.Teacherid).ToList();

                var BorrowedBooks = _libraryContext.Issuebook.Where(i => i.Studentfirstname == issuebook.Studentfirstname && i.Studentlastname == issuebook.Studentlastname && i.Classname == issuebook.Classname
                && i.Section == issuebook.Section && i.Rollnumber == issuebook.Rollnumber).ToList();
                int totalBooks = BorrowedBooks.Count();

                if(book.Count() > 4)
                {
                   return Content("Sorry, you cannot borrow more than 4 books.");
                }

                if (student.Count() != 1)
                {
                    return Content("Sorry, the student with this ID does not exist.");
                }

                if(book.Count != 1)
                {
                    return Content("Sorry, this book is not available");
                }

                if(teacher.Count != 1)
                {
                    return Content("Only valid teacher can assign the book.");
                }

                if(totalBooks < 4 && student.Count() == 1 && book.Count() == 1 && teacher.Count() == 1)
                {
                    using (libraryContext context = new libraryContext())
                    {
                        context.Issuebook.Add(issuebook);

                        //change the value of available field to false, so book is not available
                        var bookInfo = _libraryContext.Book.Where(b => b.Bookname == issuebook.Bookname && b.Bookcopynumber == issuebook.Bookcopynumber && b.available == true).FirstOrDefault();
                        context.Book.Attach(bookInfo);
                        bookInfo.available = false;
                        context.Entry(bookInfo).Property(a => a.available).IsModified = true;
						var issueBookEvent = new IssueBookIntegrationEvent(issuebook.Studentfirstname, issuebook.Studentlastname, issuebook.Bookname, issuebook.Bookcopynumber, issuebook.Issuebookdate, issuebook.Returnbookdate);

						//await _isseBookIntegrationEventService.SaveEventAndIssueBookContextChangesAsync(issueBookEvent);
						//await _isseBookIntegrationEventService.PublishThroughEventBusAsync(issueBookEvent);
						await context.SaveChangesAsync();
                        
                        return Content("The book has been issued to " + issuebook.Student + " with " + issuebook.Bookname);
                    }
                }
                else
                {
                    return Content("Please enter the correct details for issue a book.");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public bool BookAvailable(string bookName, int copynumber)
        {
            var bookInfo = _libraryContext.Book.Where(b => b.Bookname == bookName && b.Bookcopynumber == copynumber && b.available == true);

            if(bookInfo.Count() == 1)
                return true;
            else
                return false;
        }
    }
}