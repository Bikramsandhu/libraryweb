﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	//This controller is responsible to create an account for teacher, student or any teacher category and this controller is authorized to Admin category. 
	[Authorize(Policy = "RequireAdmin")]
	[Route("api/[controller]")]
    [ApiController]
    public class CreateAccountController : ControllerBase
    {
		private libraryContext _libraryContext;
		public CreateAccountController(libraryContext libraryContext)
		{
			_libraryContext = libraryContext;
		
		}

		//create Account
		[HttpPost]
		public async Task<ActionResult<Loginaccount>> CreateAccount(Loginaccount loginaccount)
		{
			try
			{
				loginaccount.Category = Category(loginaccount.Guid);
				loginaccount.CreatedOn = DateTime.Now;
				loginaccount.LastLogin = DateTime.Now;

				if (loginaccount.Username is null || loginaccount.Password is null || loginaccount.Email is null || loginaccount.Category is null)
				{
					return Content("Please fill the required information.");
				}
				else
				{
					var checkAccount = _libraryContext.Loginaccount.Where(l => l.Guid == loginaccount.Guid).FirstOrDefault();
					var checkUsername = _libraryContext.Loginaccount.Where(l => l.Username == loginaccount.Username).FirstOrDefault();
					if (checkAccount == null && checkUsername == null)
					{
						_libraryContext.Loginaccount.Add(loginaccount);
						_libraryContext.SaveChanges();
						return Content("The account has been created.");
					}
					else
					{
						return Content("This account is already existing with provided GUID number or Username is already existing, please choose another username.");
					}
					
				}
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
		}

		public string Category(Guid guid)
		{
			try
			{
				var checkStudent = _libraryContext.Student.Where(g => g.Guid == guid).FirstOrDefault();
				var checkStaff = _libraryContext.Teacher.Where(g => g.Guid == guid).FirstOrDefault();

				if(checkStaff != null)
				{
					var category = _libraryContext.Teacher.Where(g => g.Guid == guid).FirstOrDefault().DepartmentId;

					if(category == 1)
					{
						return "Admin";
					}

					if (category == 2)
					{
						return "Teacher";
					}

					if (category == 3)
					{
						return "Librarian";
					}
				}
				 if(checkStudent != null)
				{
					return "student";
				}
				else
				{
					return "No category";
				}
				
			}
			catch (Exception ex)
			{
				return ex.ToString();
			}
		}

		//This method retrieve all accounts
		[HttpGet]
		public async Task<ActionResult<List<Loginaccount>>> GetAllAccounts()
		{
			try
			{
				var accountList = _libraryContext.Loginaccount.ToList();

				if(accountList != null)
				{
					return accountList;
				}
				else
				{
					return Content("There is no account existing.");
				}
				
			}catch(Exception ex)
			{
				return Content(ex.ToString());
			}
		}

		//This method is responsible for searching an account with username
		[HttpGet]
		[Route("GetAccountByUsername/{username}")]
		public async Task<ActionResult<Loginaccount>> GetAccountByUsername(string username)
		{
			try
			{
				using(var l = _libraryContext)
				{
					Loginaccount loginaccount = l.Loginaccount.Where(g => g.Username == username).FirstOrDefault();

					if(loginaccount != null)
					{
						return loginaccount;
					}
					else
					{
						return Content("There is no account available with this username.");
					}
					
				}
			}catch(Exception ex)
			{
				return Content(ex.ToString());
			}
		}
		//This method is responsible for searching an account with GUID
		[HttpGet]
		[Route("GetAccountByGUID/{guid}")]
		public async Task<ActionResult<Loginaccount>> GetAccountByGUID(Guid guid)
		{
			try
			{
				using (var l = _libraryContext)
				{
					Loginaccount loginaccount = l.Loginaccount.Where(g => g.Guid == guid).FirstOrDefault();

					if (loginaccount != null)
					{
						return loginaccount;
					}
					else
					{
						return Content("There is no account available with this GUID.");
					}

				}
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
		}
		//This method is responsible for searching an account with email
		[HttpGet]
		[Route("GetAccountByEmail/{email}")]
		public async Task<ActionResult<Loginaccount>> GetAccountByEmail(string email)
		{
			try
			{
				using (var l = _libraryContext)
				{
					Loginaccount loginaccount = l.Loginaccount.Where(g => g.Email== email).FirstOrDefault();

					if (loginaccount != null)
					{
						return loginaccount;
					}
					else
					{
						return Content("There is no account available with this email.");
					}

				}
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
		}

		//This method is responsible for searching an account with category
		[HttpGet]
		[Route("GetAccountByCategory/{category}")]
		public async Task<ActionResult<Loginaccount>> GetAccountByCategory(string category)
		{
			try
			{
				using (var l = _libraryContext)
				{
					Loginaccount loginaccount = l.Loginaccount.Where(g => g.Category == category).FirstOrDefault();

					if (loginaccount != null)
					{
						return loginaccount;
					}
					else
					{
						return Content("There is no account available with this category.");
					}

				}
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
		}

		//this method is responsible to deleting the loginaccount
		[HttpDelete]
		[Route("DeleteAccount/{guid}")]
		public async Task<ActionResult<Loginaccount>> DeleteAccount(Guid guid)
		{
			try
			{
				var deleteAccount = _libraryContext.Loginaccount.Where(g => g.Guid == guid).FirstOrDefault();

				if(deleteAccount != null)
				{
					_libraryContext.Loginaccount.Remove(deleteAccount);
					await _libraryContext.SaveChangesAsync();
					return Content("This account has been removed successfully.");
				}
				else
				{
					return Content("This account is not existing.");
				}
			}
			catch(Exception ex)
			{
				return Content(ex.ToString());
			}
		}		
	}


}