﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http2.HPack;
using Microsoft.EntityFrameworkCore;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	//[Authorize(Policy = "RequireTeacher")]
	[Route("api/[Controller]")]
	[ApiController]
	public class StudentController : ControllerBase
	{
		private readonly libraryContext _context;

		public StudentController(libraryContext context)
		{
			_context = context;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Student>>> GetAllStudents()
		{
			var _student = await _context.Student.ToListAsync();
			return _student;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetStudent([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				throw new Exception("This is not valid request");
				//return BadRequest(ModelState);
			}

			var student = await _context.Student.FindAsync(id);

			if (student == null)
			{
				//return NotFound();
				throw new Exception("There is no student found");
			}

			return Ok(student);
		}

		[Authorize(Policy="RequireAdmin")]
		[HttpPost]
		public async Task<ActionResult<Student>> AddStudent(Student student)
		{
			var guid = Guid.NewGuid();
			student.Guid = guid;
			if (student.Studentfirstname == null || student.Studentlastname == null ||
				student.Gender == null || student.Emailaddress == null || student.Emailaddress == null ||
				student.Mobilenumber == null || student.Homenumber == null || student.Fatherfirstname == null ||
				student.Fatherlastname == null || student.Motherfirstname == null || student.Motherlastname == null ||
				student.Parenthomecontact == null || student.Parentmobilecontact == null || student.Address == null ||
				student.Village == null || student.Note == null || student.Section == null || student.Classname == null ||
				student.Rollnumber == null || student.Guid == null)
			
				throw new Exception("Please fill the required field.");
			else
				try
				{
					//verfiy whether student is already existing
					var checkStudent = _context.Student.Where(c => c.Studentfirstname == student.Studentfirstname && c.Studentlastname == student.Studentlastname && c.Rollnumber == student.Rollnumber 
					&& c.Classname == student.Classname && c.Section == student.Section).ToList();



					if(checkStudent.Count is 0)
					{
						_context.Student.Add(student);
						await _context.SaveChangesAsync();

						return CreatedAtAction(nameof(student), new { id = student.Studentid, student });
					}
					else
					{
						return Content("This student is already existing.");
					}				
				}
				catch (Exception ex)
				{
					throw new Exception("There is an error." + ex.ToString());
				}			
		}
		[Authorize(Policy = "RequireAdmin")]
		[HttpPut("{id}")]
		public async Task<ActionResult> UpdateStudent(int id, Student student)
		{
			
		   var studentid = await _context.Student.FindAsync(id);

		 
			    try
				{

				if (studentid.Studentid == id)
				   {
					return BadRequest();
				   }

					if (!ModelState.IsValid)
				   {
					 throw new Exception("student is not valid.");
				   }

				_context.Entry(student).State = EntityState.Modified;
					await _context.SaveChangesAsync();

					return Ok();
				}
				catch
				{
					throw new Exception("An error occured to updating the student.");
				}		
		}


		[Authorize(Policy = "RequireAdmin")]
		[HttpDelete("{id}")]
		 public async Task<IActionResult> DeleteStudent([FromRoute]int studentId)
		 {
			var student = await _context.Student.FindAsync(studentId);

			if(student == null)
			{
				return NotFound();
			}

			try
			{
				_context.Student.Remove(student);
				await _context.SaveChangesAsync();

				return NoContent();
			}
			catch
			{
				throw new Exception("The student could not be deleted.");
			}
		}


		//This method use to search for student with firstname and lastname and date of birth
		[HttpGet]
		[Route("{FirstName}/{LastName}/{DOB}")]
		public async Task<IActionResult> SearchStudentWithName(string FirstName, string LastName, DateTime DOB)
		{
			try
			{
				using (var dbContext = _context)
				{

					var studentInfo = _context.Student
				.Where(s => s.Studentfirstname == FirstName && s.Studentlastname == LastName && s.Dob == DOB)
				.Select(i => new { i.Studentfirstname, i.Studentmiddlename, i.Studentlastname, i.Fatherfirstname, i.Fatherlastname, i.Emailaddress, i.Motherfirstname, i.Motherlastname, i.Classname, i.Section, i.Rollnumber, i.Dob }).ToList();
					int studentsResult = studentInfo.Count();
					if (studentsResult > 1)
					{
						return Ok(studentInfo);
					}
					else
					{
						return Ok("The student does not exist with " + FirstName + " " + LastName + " " + DOB);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}			
		}

		
		//user wants to search all students with village name
		[HttpGet]
		[Route("StudentForSpecificVillage/{villageName}")]
		public async Task<(IList,string)> StudentForSpecificVillage(string villageName)
		{
			try
			{
				using (var dbContext = _context)
				{
					var studentInfo = dbContext.Student
						.Where(s => s.Village == villageName)
						.Select(i => new { i.Studentfirstname, i.Studentmiddlename, i.Studentlastname, i.Fatherfirstname, i.Fatherlastname, i.Emailaddress, i.Motherfirstname, i.Motherlastname, i.Classname, i.Section, i.Rollnumber, i.Dob,i.Village }).ToList();

					int students = studentInfo.Count();

					if (students != 0)
						return (studentInfo, students.ToString());
					else
						return (studentInfo,"There are no students from " + villageName + ".");
				}
				
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}
		}


		//find student with name and village name and this method will extract the full name into first name, last name or first name or firstname and lastname and middle name
		[HttpGet]
		[Route("FindStudentNameVillage/{studentName}/{villageName}")]
		public async Task<(IList,string)> FindStudentNameVillage (string studentName, string villageName)
		{
			string firstName ="";
			string middlename="";
			string lastName="";
			var studentsInfo = (IList)null;
			var names = studentName.Split(' ');


				using (var context = _context)
				{
				if (names.Length == 1)
				{
					 firstName = names[0];
					  studentsInfo = context.Student
						.Where(s => (s.Studentfirstname == firstName && s.Village == villageName))
						.Select(i => new { i.Studentfirstname, i.Studentmiddlename, i.Studentlastname, i.Fatherfirstname, i.Fatherlastname, i.Emailaddress, i.Motherfirstname, i.Motherlastname, i.Classname, i.Section, i.Rollnumber, i.Dob, i.Village }).ToList();

				}
				else if (names.Length == 2)
				{
					firstName = names[0];
					lastName = names[1];
					 studentsInfo = context.Student
					   .Where(s => s.Studentfirstname == firstName && s.Studentlastname == lastName && s.Village == villageName)
					   .Select(i => new { i.Studentfirstname, i.Studentmiddlename, i.Studentlastname, i.Fatherfirstname, i.Fatherlastname, i.Emailaddress, i.Motherfirstname, i.Motherlastname, i.Classname, i.Section, i.Rollnumber, i.Dob, i.Village }).ToList();
				}
				else if (names.Length > 2)
				{
					firstName = names[0];
					middlename = names[1];
					lastName = names[2];
					 studentsInfo = context.Student
					   .Where(s => (s.Studentfirstname == firstName && s.Studentlastname == lastName && s.Studentmiddlename == middlename && s.Village == villageName))
					   .Select(i => new { i.Studentfirstname, i.Studentmiddlename, i.Studentlastname, i.Fatherfirstname, i.Fatherlastname, i.Emailaddress, i.Motherfirstname, i.Motherlastname, i.Classname, i.Section, i.Rollnumber, i.Dob, i.Village }).ToList();
				}

				int numbers = studentsInfo.Count;

					if (numbers != 0)
						return (studentsInfo,numbers.ToString());
					else
						return (studentsInfo, "There");
				}
			
		}

		//search students with roll number, class and section
		[HttpGet]
		[Route("searchStudentsWithRollNumber/{rollnumber}/{classname}/{section}")]
		public async Task<IActionResult> searchStudentsWithRollNumber(int rollnumber, string classname, string section)
		{
			using (var dbContext = _context)
			{
				try
				{
					var studentInfo = dbContext.Student
						.Where(s => s.Rollnumber == rollnumber && s.Classname == classname && s.Section == section)
						.Select(i => new { i.Studentfirstname, i.Studentlastname, i.Dob, i.Classname, i.Section, i.Fatherfirstname, i.Fatherlastname, i.Village }).ToList();

					if(studentInfo.Count() != 0)
					{
						return Ok(studentInfo);
					}
					else
					{
						return Content("The student does not exist with rollnumber: " + rollnumber + ", class: " + classname + " , section:" + section + ".");
					}
				}
				catch (Exception ex)
				{
					throw new Exception(ex.ToString());
				}
			}
		}

		//find all students with classname and section
		[HttpGet]
		[Route("searchStudentsWithClassSection/{classname}/{section}")]
		public async Task<(IList, string)> searchStudentsWithClassSection(string classname, string section)
		{
			try
			{
				using (var context = _context)
				{
					var studentInfo = context.Student
						.Where(s => s.Classname == classname && s.Section == section)
						.Select(i => new { i.Studentfirstname, i.Studentlastname, i.Dob, i.Classname, i.Section, i.Fatherfirstname, i.Fatherlastname, i.Village }).ToList();

					int totalStudentsInClass = studentInfo.Count();

					if(totalStudentsInClass != 0)
					{
						return (studentInfo, totalStudentsInClass.ToString());
					}
					else
					{
						return (studentInfo,"There are no students exist with class:" + classname + " and section: " + section);
					}
				}
			}catch(Exception ex)
			{
				throw new Exception(ex.ToString());
			}
		
		}

		//find all students in class with classname
		[HttpGet]
		[Route("searchStudentsWithClass/{classname}")]
		public async Task<(IList, string)> searchStudentsWithClass(string classname)
		{
			try
			{
				using (var context = _context)
				{
					var studentInfo = context.Student
						.Where(s => s.Classname == classname)
						.Select(i => new { i.Studentfirstname, i.Studentlastname, i.Dob, i.Classname, i.Section, i.Fatherfirstname, i.Fatherlastname, i.Village }).ToList();

					int totalStudentsInClass = studentInfo.Count();

					if (totalStudentsInClass != 0)
					{
						return (studentInfo, totalStudentsInClass.ToString());
					}
					else
					{
						return (studentInfo, "There are no students exist with class:" + classname);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}

		}
	}
}