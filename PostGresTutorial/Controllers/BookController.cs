﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
		private libraryContext _dbContext;

		public BookController (libraryContext context)
		{
			_dbContext = context;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Book>>> GetAllBooks()
		{
			var _book = await _dbContext.Book.ToListAsync();
			return _book;
		}

		[HttpGet("{id}")]
		public async Task<Book>GetBookById(int id)
		{
			var book = await _dbContext.Book.FindAsync(id);

			if(book == null)
			{
				throw new Exception("The book does not exist with this " + id + "Id.");
			}

			return book;
		}

		[HttpGet]
		[Route("GetBookByBookname/{bookname}")]
		public async Task<IActionResult> GetBookByBookname(string bookname)
		{
			IList<Book> books = _dbContext.Book.
				Where(t => t.Bookname == bookname).ToList();

			int booksResults = books.Count();

			if(booksResults != 0)
			{
				return Ok(books.ToList());
			}
			else
			{
				return Ok("There is not book exist with this bookname " + bookname);
			}
		}

		[HttpGet]
		[Route("GetBookByAuthor/{author}")]
		public async Task<IActionResult> GetBookByAuthor(string author)
		{
			IList<Book> books = _dbContext.Book.
				Where(s => s.AuthorName == author).ToList();

			int booksNumber = books.Count();

			if(booksNumber != 0)
			{
				return Ok(books);
			}
			else
			{
				return Ok("There is no book availble with " + author + " author name.");
			}
		}

		[HttpGet]
		[Route("GetBookByPublisher/{publisher}")]
		public async Task<IActionResult> GetBookByPublisher(string publisher)
		{
			IList<Book> books = _dbContext.Book.
				Where(b => b.Publishername == publisher).ToList();

			int numbers = books.Count();

			if(numbers != 0)
			{
				return Ok(books);
			}
			else
			{
				return Ok("There is no book available with publisher name " + publisher + " publisher name.");
			}
		}

		[HttpGet]
		[Route("GetBookByBooktype/{type}")]
		public async Task<IActionResult> GetBookByBooktype(string type)
		{
			IList<Book> books = _dbContext.Book.
				Where(b => b.Booktype == type).ToList();

			int numbers = books.Count();

			if (numbers != 0)
			{
				return Ok(books);
			}
			else
			{
				return Ok("There is no book available with publisher name " + type + " publisher name.");
			}
		}

		[HttpGet]
		[Route("GetBookByAuthorPublisherTitle/{title}/{Publisher}/{Author}/{bookType}")]
		public async Task<IActionResult> GetBookByAuthorPublisherTitle(string title, string publisher, string author, string bookType)
		{
			IList<Book> books = _dbContext.Book
				.Where(s => s.Bookname == title && s.Publishername == publisher && s.AuthorName == author && s.Booktype == bookType).ToList();

			int numbers = books.Count();

			if(numbers != 0)
			{
				return Ok(books);
			}
			else
			{
				return Ok("There is no book available with " + title  + publisher + " and author " + author);
			}
		}

		[Authorize(Policy = "RequireLibrarian")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteById(int id)
		{
			var deletebook = await _dbContext.Book.FirstOrDefaultAsync(s => s.Bookid == id);
			try
			{
				if (deletebook == null)
				{
					return Content("This Book does not exist.");
				}
				else
				{
					_dbContext.Book.Remove(deletebook);
					await _dbContext.SaveChangesAsync();
					return Content("The book has been deleted.");
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}

		}

		[Authorize(Policy = "RequireLibrarian")]
		[HttpPut("{id}")]
		public async Task<IActionResult> UpdateBookResult(int id, Book Updatebook)
		{
			var book = await _dbContext.Book.FirstOrDefaultAsync(s => s.Bookid == id);


			if(book == null)
			{
				return Content("This book does not exist.");
			}
			else
			{
				try
				{
					_dbContext.Entry(Updatebook).State = EntityState.Modified;
					await _dbContext.SaveChangesAsync();
					return Content("The book has been updated.");
				}
				catch (Exception ex)
				{
					return Content(ex.ToString());
				}
				
			}
		}

		[Authorize(Policy = "RequireLibrarian")]
		//[Authorize(Policy = "RequireAdmin")]
		[HttpPost]
		public async Task<IActionResult> AddBook(Book book)
		{
			if (book.Bookname == null || book.AuthorName == null || book.Booktype == null || book.Publishername == null)
			{
				return Content("Please fill all the fields");
			}

		    var bookNameCopy = _dbContext.Book.Where(b => b.Bookname == book.Bookname && b.Bookcopynumber == book.Bookcopynumber).ToList();
			int numbers = bookNameCopy.Count();

			if(numbers == 1)
			{
				return Content("This book is already existing, please enter this book with different copy number.");
			}

			try
			{
				_dbContext.Book.Add(book);
				await _dbContext.SaveChangesAsync();

				return Content("A book has been added with Bookname" + book.Bookname + " and with copy number of " + book.Bookcopynumber + ".");
			}
			catch
			{
				throw new Exception("There is an error.");
			}
		}
	}
}