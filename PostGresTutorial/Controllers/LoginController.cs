﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PostGresTutorial.Model;
using ServiceStack.Configuration;

namespace PostGresTutorial.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
		private IConfiguration _config;
		private libraryContext _libraryContext;

		public LoginController(IConfiguration config, libraryContext libraryContext)
		{
			_config = config;
			_libraryContext = libraryContext;
		}

		[AllowAnonymous]
		[HttpPost]
		public IActionResult Login([FromBody]Loginaccount loginaccount)
		{
			try
			{
				var user = _libraryContext.Loginaccount.SingleOrDefault(u => u.Username == loginaccount.Username);

				if (user == null)
				{
					return Content("There is no user available with this username.");
				}
				else if (user.Password == loginaccount.Password)
				{
					var tokenString = GenerateJSONWebToken(user);
					UpdateLastTimeLogin(loginaccount);
					return Ok(new { token = tokenString });	
				}
				else
				{
					return Content("Please put the correct password for: " + loginaccount.Username);
				}
			} catch(Exception ex)
			{
				return Content(ex.ToString());
			}
		
		}

		private string GenerateJSONWebToken(Loginaccount user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.Name, user.Username),
				new Claim("teacherCategory", user.Category),
				//new Claim(ClaimTypes.Role = "teacher",user.Category)
			};

			var token = new JwtSecurityToken(_config["Jwt:Issuer"],
				_config["Jwt:Issuer"],
				claims:claims,
				expires:DateTime.Now.AddMinutes(120),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		private void UpdateLastTimeLogin(Loginaccount lastlogin)
		{
			using(var db = new libraryContext())
			{
				var LoginInfo = db.Loginaccount.Where(u => u.Username == lastlogin.Username).FirstOrDefault();
				LoginInfo.LastLogin = DateTime.Now;
				db.Entry(LoginInfo).Property(x => x.LastLogin).IsModified = true;
				db.SaveChanges();
			}
		}

	}
}