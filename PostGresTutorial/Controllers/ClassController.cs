﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Logger;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGresTutorial.Logger;
using PostGresTutorial.Model;

namespace PostGresTutorial.Controllers
{
	//[Authorize("Admin")]
	[Authorize(Policy = "RequireTeacher")]
	[Route("api/[controller]")]
	[ApiController]
	public class ClassController : ControllerBase
	{
		
		private readonly libraryContext _context;
		private ILoggerManager _logger;

		public ClassController(libraryContext libraryContext, ILoggerManager logger)
		{
			_context = libraryContext;
			_logger = logger;
		}


		[HttpGet]
		public async Task<ActionResult<IEnumerable<Class>>> Classes()
		{
				_logger.LogInfo("Fetching All Classes");

				var  _classes = await _context.Class.ToListAsync();
			   _logger.LogInfo($"Returning {_classes.Count} classes.");

				return _classes;	
		}

		[HttpGet("{id}")]
		public async Task<ActionResult<Class>> GetClassInfo(int id)
		{

			var classInfo = await _context.Class.FindAsync(id);

			if (classInfo == null)
			{
				//return NotFound();
				throw new Exception("item is not found");
			}

			return classInfo;
		}

		[HttpGet]
		[Route("{classnameSearch}/{sectionSearch}")]
		//method for querying the class capacity by searchine class with section
		public async Task<ActionResult<Class>> ClassSearch(string classnameSearch, string sectionSearch)
		{
			try
			{
				using (var context = new libraryContext())
				{
					var ClassExists = context.Class
						.Where(c => c.Classname == classnameSearch && c.Section == sectionSearch).FirstOrDefault();

					if (ClassExists == null)
					{
						return BadRequest("This class with this section does not exist");
					}


					var capacity = context.Class
						.Where(c => c.Classname == classnameSearch && c.Section == sectionSearch)
						.Select(c => c.Capacity).FirstOrDefault();

					return Ok(capacity);					
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}			
		}

		[HttpGet]
		[Route("{classname}")]
		public async Task<ActionResult<Class>> TotalStudentsInClass(string className)
		{
			try
			{
				using (var context = new libraryContext())
				{
					var ClassExists = context.Class
						.Where(c => c.Classname == className).FirstOrDefault();

					if (ClassExists == null)
					{
						return Ok("This class  does not exist");
					}


					var capacity = context.Class
						.Where(c => c.Classname == className)
						.Select(c => new{c.Section, c.Capacity}).ToList();

					

					return Ok(capacity);
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.ToString());
			}
		}

		[Authorize(Policy = "RequireAdmin")]
		[HttpPost]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<Class>> PostClass(Class pclass)
		{

			try
			{
				_context.Class.Add(pclass);
				await _context.SaveChangesAsync();

				return CreatedAtAction(nameof(GetClassInfo), new { id = pclass.Classid }, pclass);
		     }catch(Exception ex)
			{
				throw new Exception(ex.ToString());
	        }

         }

		[HttpPut("{id}")]
		public async Task<ActionResult> PutClassName(int id, Class pclass)
		{

			if (id != pclass.Classid)
			{
				return BadRequest();
			}

			_context.Entry(pclass).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return Ok();
		}

		[HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClass(int id)
        {
            var Class = await _context.Class.FindAsync(id);

            if(Class == null)
            {
				var message = string.Format("Class with ID = {0} not found", id);
                return NotFound();
            }

            _context.Class.Remove(Class);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}