﻿using Microsoft.AspNetCore.Authorization;
using PostGresTutorial.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PostGresTutorial.AuthorizationHandler
{
	public class TeacherRequirement : IAuthorizationRequirement
	{
		public string _studentClass { get; set; }
		public TeacherRequirement(string studentClass)
		{
			_studentClass = studentClass;
		}
	}

	public class TeacherHandler :
		AuthorizationHandler<TeacherRequirement>
	{
		libraryContext _libraryContext;
		protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TeacherRequirement requirement)
		{
			if (!context.User.HasClaim(c => ClaimTypes.Role == "teacher"))
			{
				return Task.FromResult(0);
			}

			//return 


			if (requirement._studentClass == "11th" || requirement._studentClass == "12th")
			{
				context.Succeed(requirement);
			}
			return Task.FromResult(0);
		}
	}

}
