﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using EventBus1.Abstractions;
using EventBus1.Events;
using IntegrationEventLogEF.Services;
using IntegrationEventLogEF.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PostGresTutorial.Model;

namespace PostGresTutorial.Event
{
    public class IssuebookIntegrationEventService : IIsseBookIntegrationEventService
    {
        private readonly Func<DbConnection, IIntegrationEventLogService> _integrationEventService;
        private readonly IEventBus _eventBus;
        private readonly libraryContext _libraryContext;
        private readonly IIntegrationEventLogService _eventLogService;
        private readonly ILogger<IIsseBookIntegrationEventService> _logger;

        public IssuebookIntegrationEventService(
            ILogger<IssuebookIntegrationEventService> logger,IEventBus eventBus,libraryContext libraryContext,Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _libraryContext = libraryContext ?? throw new ArgumentNullException(nameof(libraryContext));
            _integrationEventService = integrationEventLogServiceFactory ?? throw new ArgumentNullException(nameof(integrationEventLogServiceFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _eventLogService = _integrationEventService(_libraryContext.Database.GetDbConnection());
        }
        
        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
                _logger.LogInformation("----Publishing integration event: {IntegrationEventId}");

                await _eventLogService.MarkEventAsInProgressAsync(evt.Id);
                _eventBus.Publish(evt);
                await _eventLogService.MarkEventAsPublishedAsync(evt.Id);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", evt.Id, Program.AppName, evt);
                await _eventLogService.MarkEventAsFailedAsync(evt.Id);
            }
        }

        public async Task SaveEventAndIssueBookContextChangesAsync(IntegrationEvent evt)
        {
            _logger.LogInformation("------IssueBookUntegrationEventService - Saving changes and integrationEvent:{IntegrationEventId}", evt.Id);
            // await ResilientTransaction.
            await ResilientTransaction.New(_libraryContext).ExecuteAsync(async () => 
            {
                await _libraryContext.SaveChangesAsync();
                await _eventLogService.SaveEventAsync(evt, _libraryContext.Database.CurrentTransaction);
            });
        }
    }
}
