﻿using EventBus1.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGresTutorial.Event
{
    public class IssueBookIntegrationEvent : IntegrationEvent
    {
        public string Studentfirstname { get; private set; }
        public string Studentlastname { get; private set; }
        public string Bookname { get; set; }
        public int Bookcopynumber { get; set; }
        public DateTime Issuebookdate { get; set; }
        public DateTime Returnbookdate { get; set; }

        public IssueBookIntegrationEvent(string studentfirstname, string studentlastname, string bookName, int bookcopynumber, DateTime issuebookdate, DateTime returnbookdate)
        {
            Studentfirstname = studentfirstname;
            Studentlastname = studentlastname;
            Bookname = bookName;
            Bookcopynumber = bookcopynumber;
            Issuebookdate = issuebookdate;
            Returnbookdate = returnbookdate;
        }
    }
}
