﻿using EventBus1.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGresTutorial.Event
{
    public interface IIsseBookIntegrationEventService
    {
        Task SaveEventAndIssueBookContextChangesAsync(IntegrationEvent evt);
        Task PublishThroughEventBusAsync(IntegrationEvent evt);
    }
}
