﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using EventBus1;
using EventBus1.Abstractions;
using EventBusRabbitMQ;
using IntegrationEventLogEF;
using IntegrationEventLogEF.Services;
using Logger;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.Web.CodeGeneration;
using NLog;
using NLog.Extensions.Logging;
using PostGresTutorial.AuthorizationHandler;
using PostGresTutorial.CustomExceptionMiddleware;
using PostGresTutorial.Event;
using PostGresTutorial.Extensions;
using PostGresTutorial.Logger;
using PostGresTutorial.Model;
using PostGresTutorial.Repository;
using RabbitMQ.Client;

namespace PostGresTutorial
{
    public class Startup
    {

		public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
			 LogManager.LoadConfiguration(String.Concat( "../Logger/nlog.config")); 
              Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<libraryContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DBconnString")));
			services.ConfigureLoggerService();
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<IntegrationEventLogContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DBconnString"),
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            });

			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options => {
					options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
					{
						ValidateIssuer = true,
						ValidateAudience = true,
						ValidateLifetime = true,
						ValidateIssuerSigningKey = true,
						ValidIssuer = Configuration["Jwt:Issuer"],
						ValidAudience = Configuration["Jwt:Issuer"],
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
					};
				});

			services.AddAuthorization(options =>
			{
				options.AddPolicy("RequireTeacher", policy => policy.RequireClaim("teacherCategory", "admin", "teacher", "librarian"));
				options.AddPolicy("RequireAdmin", policy => policy.RequireClaim("teacherCategory", "Admin"));
				options.AddPolicy("RequireLibrarian", policy => policy.RequireClaim("teacherCategory", "librarian", "Admin"));
			});
			services.AddSingleton<IAuthorizationHandler,TeacherHandler>();
			services.AddIntegrationServices(Configuration);
			services.AddEventBus(Configuration);
			var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerManager logger)
        {

            if (env.IsDevelopment())
            {				
				app.UseDeveloperExceptionPage();
            }
            else
            {			
				app.UseHsts();
            }
			//app.ConfigureExceptionHandler(logger);
			app.ConfigureCustomExceptionMiddleware();
			app.UseDefaultFiles();
            app.UseStaticFiles();
			app.UseHttpsRedirection();
			//app.UseMvc();
			app.UseAuthentication();
			app.UseMvc(routes =>
			{
				//routes.MapRoute(
				//	  name: "default",
				//	  template: "{controller=Class}/{action=TotalStudentsInClass}/{className}");

				//routes.MapRoute(
				//	name: "capacity",
				//	template: "api/{controller=Class}/{action=ClassSearch}/{classnameSearch}/{sectionSearch}");
			});
		}
    }

	public static class CustomExtensionMethods
	{
		public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddTransient<Func<DbConnection, IIntegrationEventLogService>>(
				sp => (DbConnection c) => new IntegrationEventLogService(c));

			services.AddTransient<IIsseBookIntegrationEventService, IssuebookIntegrationEventService>();

			services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
			{
				var settings = sp.GetRequiredService<IOptions<LibrarySettings>>().Value;
				var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();


				var factory = new ConnectionFactory()
				{
					HostName = "localhost",
					UserName = "guest",
					Password = "guest",
					AutomaticRecoveryEnabled = true,

					DispatchConsumersAsync = true
				};
				var retryCount = 5;

				//if (!string.IsNullOrEmpty(configuration["EventBusUserName"]))
				//{
				//	factory.UserName = configuration["EventBusUserName"];
				//}

				//if (!string.IsNullOrEmpty(configuration["EventBusPassword"]))
				//{
				//	factory.Password = configuration["EventBusPassword"];
				//}

				//var retryCount = 5;
				//if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
				//{
				//	retryCount = int.Parse(configuration["EventBusRetryCount"]);
				//}

				return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
			});
			return services;
		}

		public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
		{
			var subscriptionClientName = configuration["SubscriptionClientName"];

			services.AddSingleton<IEventBus, EventBusRabbitMQ.EventBusRabbitMQ>(sp =>
			{
				var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
				var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
				var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ.EventBusRabbitMQ>>();
				var eventBusSubscriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

				var retryCount = 5;
				if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
				{
					retryCount = int.Parse(configuration["EventBusRetryCount"]);
				}

				return new EventBusRabbitMQ.EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubscriptionsManager, subscriptionClientName, retryCount);
			});

			services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
			return services;
		}
	}
}
