﻿using EmailNotification.Data;
using EmailNotification.Events;
using EmailNotification.Model;
using EventBus1.Abstractions;
using Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotification.EventHandler
{
    public class IssueBookIntegrationEventHandler : IIntegrationEventHandler<IssueBookIntegrationEvent>
    {
        DbEmailContext _DbEmailContext;
        LoggerManager _logger;
        public IssueBookIntegrationEventHandler(DbEmailContext context, LoggerManager logger)
        {
            _logger = logger;
            _DbEmailContext = context;
        }
        public async Task Handle(IssueBookIntegrationEvent @event)
        {
            await addIssuedBookInformation(@event.Studentfirstname, @event.Studentlastname, @event.Bookname, @event.Bookcopynumber, @event.Issuebookdate, @event.Returnbookdate);
        }

        private async Task addIssuedBookInformation(string studentFirstName, string studentLastName, string bookName, int bookCopyNumber, DateTime issueBookDate, DateTime returnBookDate)
        {
            var book = new IssueBookInfo { Studentfirstname = studentFirstName, Studentlastname = studentLastName, Bookname = bookName, Bookcopynumber = bookCopyNumber, Issuebookdate = issueBookDate, Returnbookdate = returnBookDate};
            try
            {
                _DbEmailContext.issueBookInfos.Add(book);
                await _DbEmailContext.SaveChangesAsync();
                _logger.LogInfo("Issued Book has been added to database.");
            }catch (Exception ex)
            {
                _logger.LogWarn(ex.Message);
                throw new Exception(ex.ToString());
            }               
        }
    }
}
