﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotification.Model
{
	public class Student
	{
		[Key]
		public int Studentid { get; set; }
		public string Studentfirstname { get; set; }
		public string Studentlastname { get; set; }
		public string Gender { get; set; }
		public string Emailaddress { get; set; }
	}
}
