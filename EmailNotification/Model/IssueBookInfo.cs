﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotification.Model
{
	public class IssueBookInfo
	{
		[Key]
		public int Issuebookid { get; set; }
		public DateTime Issuebookdate { get; set; }
		public DateTime Returnbookdate { get; set; }
		public string Studentfirstname { get; set; }
		public string Studentlastname { get; set; }
		public string Bookname { get; set; }
		public int Bookcopynumber { get; set; }
	}
}
