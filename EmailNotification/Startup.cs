﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using EmailNotification.Data;
using EmailNotification.EventHandler;
using EmailNotification.Events;
using EventBus1;
using EventBus1.Abstractions;
using EventBusRabbitMQ;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace EmailNotification
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
			services.AddEntityFrameworkNpgsql().AddDbContext<DbEmailContext>().BuildServiceProvider();

			services.AddSingleton<IRabbitMQPersistentConnection>(sp => 
			{
				var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

				var factory = new ConnectionFactory()
				{
					HostName = "localhost",
					UserName = "guest",
					Password = "guest",
					DispatchConsumersAsync = true
				};
				var retryCount = 5;
				return new DefaultRabbitMQPersistentConnection(factory, logger,retryCount);
			});
			RegisterEventBus(services);
		}

		

		private void RegisterEventBus(IServiceCollection services)
		{
			var subscriptionClientName = Configuration["SubscriptionClientName"];
			services.AddSingleton<IEventBus,EventBusRabbitMQ.EventBusRabbitMQ>(sp => 
			{
				var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
				var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
				var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ.EventBusRabbitMQ>>();
				var eventBusSubscriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
				var retryCount = 5;

				return new EventBusRabbitMQ.EventBusRabbitMQ(rabbitMQPersistentConnection, logger,iLifetimeScope,eventBusSubscriptionsManager,subscriptionClientName,retryCount);
			});

			services.AddSingleton<IEventBusSubscriptionsManager,InMemoryEventBusSubscriptionsManager>();
			services.AddTransient<IssueBookIntegrationEventHandler>();
		}

		private void ConfigureEventBus(IApplicationBuilder app)
		{
			var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
			eventBus.Subscribe<IssueBookIntegrationEvent, IssueBookIntegrationEventHandler>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseMvc();
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<IssueBookIntegrationEvent, IssueBookIntegrationEventHandler>();
		}
        
	}
}
