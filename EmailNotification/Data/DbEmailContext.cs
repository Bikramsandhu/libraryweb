﻿using EmailNotification.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailNotification.Data
{
	public class DbEmailContext : DbContext
	{
		public DbSet<IssueBookInfo> issueBookInfos { get; set; }
		public DbSet<Student> students { get; set; }


		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseNpgsql("host=localhost;Port=5432;Database=NotificationDatabase;Username=postgres;Password=Bikramsandhu1;");
		}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
		{

			
		}
	}
}
