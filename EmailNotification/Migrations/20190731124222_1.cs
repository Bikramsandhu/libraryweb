﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EmailNotification.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "issueBookInfos",
                columns: table => new
                {
                    Issuebookid = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Issuebookdate = table.Column<DateTime>(nullable: false),
                    Returnbookdate = table.Column<DateTime>(nullable: false),
                    Studentfirstname = table.Column<string>(nullable: true),
                    Studentlastname = table.Column<string>(nullable: true),
                    Bookname = table.Column<string>(nullable: true),
                    Bookcopynumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_issueBookInfos", x => x.Issuebookid);
                });

            migrationBuilder.CreateTable(
                name: "students",
                columns: table => new
                {
                    Studentid = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Studentfirstname = table.Column<string>(nullable: true),
                    Studentlastname = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Emailaddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_students", x => x.Studentid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "issueBookInfos");

            migrationBuilder.DropTable(
                name: "students");
        }
    }
}
